
Pong

How to run:
1. Download and install Corona SDK

https://developer.coronalabs.com/downloads/coronasdk

Registration may be required (free of charge).

2. Open project, selecting 'main.lua' file from the root of folder.

3. To compile as apk select File > Build > Android. Select then Keystore as Debug, proper folder and click 'Build'.

4. In some cases font files won't open in simulator mode. You should try to install font file into the system or build apk.

About the Game:

Controls
Decks are controlled by swiping on the touch area on the bottom and top of the screen (in hot seat mode, top part is deactivated).
We don't have to touch the deck exactly. It moves at delta position, so it moves as much as we move our finger.

There are two types of gameplay, single and hot seat.

The single one is against computer directed opponent.
We are able to set 5 difficulty levels, which differ in reaction time of opponent. We may change it in 'Options' menu.
In menu we are able to turn off music and also sound. There is no such possibility to change difficulty during the game.
When we score, we hear congratulations sound. Failure sound is being played in opposed situation.
After that, we're able to play again or quit by pressing back button (hardware one).

The hot seat mode is for play against human being player (I tried to play with my agama, but with no positive result).
You control bottom part of screen and your enemy controlls are placed on upper part of the screen.
On screen notifications are flipping every few seconds, so both players may see them.

Quit game:
Press back button. Or any button on keyboard if yo are in Corona Simulator.

Gotchas:
1. "You're Winner!" is not a bug. Google it: Big Rigs: Over the Road Racing :)
2. Ball movement isn't perfect. Sometimes it horizontal to vertical ratio is to big (bouncing walls).
    I just wanted to do it with physiscs library to prove my ability. It would be much better to perform transition
    with desired angle, based on the position where ball touches the disc.
3. Sounds, additional graphics and fonts aren't so cute. But I decided to not waste much time on a things that are outside my task.
4. You may want to turn off "properties.debugMode" to deactivate rigidbodies visibility (ball, walls and discs)

Don't publish the game!!
Also don't use any part of the code. It is written only for recruitment process.