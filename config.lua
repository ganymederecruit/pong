

require("code.logging.console")
log = logging.console()
logTable = logging.logTable
shallowLogTable = logging.shallowLogTable
log.table = shallowLogTable

application =
{

    content = {
        width = 640,
        height = 960,
        scale = "letterBox",
        fps = 60,
        imageSuffix = {
            ["-hd"] = 1.4,
            [""] = 0.95,
            ["-hdpi"] = 0.7,
            ["-sd"] = 0.5,
        }
    },
		--[[
		imageSuffix =
		{
			    ["@2x"] = 2,
		},
		--]]


	--[[
	-- Push notifications
	notification =
	{
		iphone =
		{
			types =
			{
				"badge", "sound", "alert", "newsstand"
			}
		}
	},
	--]]    
}
