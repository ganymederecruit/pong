--
-- Created by IntelliJ IDEA.
-- User: maciej
-- Date: 14.10.2013
-- Time: 16:32
--

require("code.tools.icebox.ice")
local properties = require("code.config.properties")
local highResDefault = "high"
local androidDpi = display.contentScaleY
if androidDpi < 0.49 then
    highResDefault = "high"
elseif androidDpi < 0.68 then
    highResDefault = "med"
else
    androidDpi = "low"
end

local options = {
    sound = true,
    music = true,
    difficulty = 3,
}

--initialize options. New file will be created if one does not exist
options.init = function()
    options.db = ice:loadBox("options")
    local fromDb = options.db:retrieve("options") or {}
    for k, v in pairs(options) do
        if (type(v) ~= "function" and type(v) ~= "table") or k == "players" or k == "playerDefaults" or k == "auditLogs" then
            if fromDb[k] == nil then
                fromDb[k] = v
            end
            if k == "players" then
                for _, player in pairs(fromDb.players) do
                    for q, w in pairs(options.playerDefaults) do
                        if player[q] == nil then
                            player[q] = w
                        end
                    end
                end
            elseif k == "playerDefaults" then
                for q, w in pairs(options.playerDefaults) do
                    if fromDb[k][q] == nil then
                        fromDb[k][q] = w
                    elseif fromDb[k][q] ~= w then
                        fromDb[k][q] = w
                    end
                end
            end
        end
    end
    options.db:store("options", fromDb)
    options.db:save()
    options.load()
end

--load already stored data
options.load = function()
    local db = options.db
    local fromDb = db:retrieve("options")
    for k, v in pairs(fromDb) do
        if (type(v) ~= "function" and type(v) ~= "table") or k == "players" or k == "playerDefaults" or k == "auditLogs" then
            options[k] = fromDb[k]
        end
    end
end

options.save = function(arg, val)
    options[arg] = val
    local fromDb = options.db:retrieve("options")
    fromDb[arg] = val
    options.db:store("options", fromDb)
    options.db:save()
    options.load() --update options object
end

-- save & update database
options.store = function()
    options.db:save()
    options.load()
end

options.init()

return options