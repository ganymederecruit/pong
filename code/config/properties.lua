local properties = {}

properties.debugMode = true


properties.androidOS = "Android" == system.getInfo("platformName")
properties.font = "Kremlin Samovar"
properties.darkColor = { 0.227, 0.227, 0.227 }
properties.lightColor = { 0.219, 0.498 }
properties.powersColor = { 0.227, 0.8 }
properties.defaultPlayerId = "default_player"

-- device dimensions
properties.width = display.contentWidth + display.screenOriginX * -2
properties.height = display.contentHeight + display.screenOriginY * -2
properties.x = display.screenOriginX
properties.y = display.screenOriginY
properties.center = { x = properties.x + properties.width / 2, y = properties.y + properties.height / 2 }
properties.device = system.getInfo("model")
properties.widthMultiplier = properties.width / display.contentWidth

-- fonts
properties.fontTypeSamovar = "Kremlin Samovar"
properties.fontTypeCounter = "Digital Counter 7"

properties.textRgb = { 86, 82, 94, 1 }
properties.textFontSize = 50
properties.scoreFontSize = 20

-- colors
properties.colorRed = { 1, 0.6, 0.4 }
properties.colorGreen = { 0, 1, 0 }
properties.colorBlue = { 0.4, 0.6, 0.99 }
properties.colorYellow = { 1, 1, 0 }
properties.colorCyan = { 0, 1, 1 }

properties.colorsTable = {
    properties.colorRed,
    properties.colorGreen,
    properties.colorBlue,
    properties.colorYellow,
    properties.colorCyan,
}

-- audio events
properties.playAudioBounce1 = "playAudioBounce1"
properties.playAudioBounce2 = "playAudioBounce2"
properties.playAudioSuccess = "playAudioSuccess"
properties.playAudioFailure = "playAudioFailure"
properties.playAudioMusic = "playAudioMusic"
properties.stopAudioMusic = "stopAudioMusic"

-- events
properties.eventTypeButtonPressed = "eventTypeButtonPressed"
properties.eventTypeToggleButton = "eventTypeToggleButton"
properties.eventTypeOpenOptions = "eventTypeOpenOptions"
properties.eventTypeMoveTopDisc = "eventTypeMoveTopDisc"
properties.eventTypeMoveBottomDisc = "eventTypeMoveBottomDisc"
properties.eventTypeReleaseBall = "eventTypeReleaseBall"
properties.eventTypeRoundOver = "eventTypeRoundOver"
properties.eventTypeRestartLevel = "eventTypeRestartLevel"

-- difficulty level
properties.difficulty = {
    { name = "Can I play, Mom?", deltaT = 300 },
    { name = "Don't hurt me", deltaT = 250 },
    { name = "Show me what U got", deltaT = 200 },
    { name = "Am I insane?", deltaT = 120 },
    { name = "Call me UBER", deltaT = 40 },
}

properties.scorePerWin = 200

-- credits
properties.bannerTransitionTime = 300
properties.bannerStayOnTime = 3200
properties.bannerShuffleDelay = 4000

return properties