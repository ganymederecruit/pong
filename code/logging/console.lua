-------------------------------------------------------------------------------
-- Prints logging information to console
--
-- @author Thiago Costa Ponte (thiago@ideais.com.br)
--
-- @copyright 2004-2011 Kepler Project
--
-------------------------------------------------------------------------------
-- debug levels

local i = true
local e = true
local d = true

local skipFiles = {
    DEBUG = {},
    INFO = {},
    ERROR = {},
    WARN = {}
}

local gsub = string.gsub
local format = string.format

require("code.config.logging")


local function shouldSkip(file, level)
    local skip = false
    for i = 1, #skipFiles[level] do
        if "@" .. skipFiles[level][i] == file then
            skip = true
            break
        end
    end
    return skip
end

local logs = {}

local function showLogs(regular)
    if #logs > 0 then
--        print("printing logs, regular:", tostring(regular))
        print(table.concat(logs, "\n"))
        logs = nil
        logs = {}
    end
end
Runtime:addEventListener("releaseLogs", showLogs)
Runtime:addEventListener("unhandledError", showLogs)

local function checkLogs()
    if #logs > 500 then
        showLogs(true)
    end
end

function logging.console(logPattern)

    return logging.new(function(self, level, message)
        if level == "DEBUG" and not d or level == "INFO" and not i or level == "ERROR" and not e then return false end
        local t = debug.getinfo(4)
        local f = gsub(gsub(t.source, "/.*/", ""), ".lua", "")
        if shouldSkip(f, level) then return end


        table.insert(logs, logging.prepareLogMsg(logPattern, system.getTimer(), level, f, t.currentline, message))
--        print(logging.prepareLogMsg(logPattern, system.getTimer(), level, f, t.currentline, message))
        checkLogs()
        return true
    end)
end

timer.performWithDelay(2000, showLogs, 0)


logging.logTable = function(tab)
    if not i and not d then return end
    local t = debug.getinfo(2)
    local f = gsub(gsub(t.source, "/.*/", ""), ".lua", "")
    print("--------------", system.getTimer(), "--------------")
    print(format("PRINTING TABLE %s [%s:%s]", tostring(tab), f, t.currentline))
    local function logTableLocal(o, level)
        local tabs = ""

        for i = 1, level do
            tabs = tabs .. "\t"
        end
        for k, v in pairs(o) do
            if type(v) == "table" then
                print(format(tabs .. "%s \t {", tostring(k)))
                logTableLocal(v, level + 1)
                print(format(tabs .. "}"))
            else
                print(format(tabs .. "%s \t : %s", tostring(k), tostring(v)))
            end
        end
    end

    logTableLocal(tab, 1)
    print("FINISHED PRINTING TABLE")
    print("--------------")
end

logging.shallowLogTable = function(tab, maxDepth)
    if not i and not d then return end
    local t = debug.getinfo(2)
    local f = gsub(gsub(t.source, "/.*/", ""), ".lua", "")
    print("--------------", system.getTimer(), "--------------")
    print(format("SHALLOW PRINTING TABLE %s [%s:%s]", tostring(tab), f, t.currentline))
    local function logTableLocal(o, level, depth)
        local tabs = ""

        for i = 1, level do
            tabs = tabs .. "\t"
        end
        for k, v in pairs(o) do
            if type(v) == "table" then
                print(format(tabs .. "%s \t {", tostring(k)))
                if depth > 0 then logTableLocal(v, level + 1, depth - 1) end
                print(format(tabs .. "}"))
            else
                print(format(tabs .. "%s \t : %s", tostring(k), tostring(v)))
            end
        end
    end

    logTableLocal(tab, 1, maxDepth)
    print("SHALLOW FINISHED PRINTING TABLE")
    print("--------------")
end

return logging.console

