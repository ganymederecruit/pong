--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 11.06.2015
-- Time: 23:07
-- You deserve a coffee!
--

local tools = require("code.tools")
local properties = require("code.config.properties")

local ballObject = {}

ballObject.new = function()
    local ball = display.newGroup()
    local ballImg = tools.img("game_ball_001.png", 40, 40)
    ball:insert(ballImg)

    local lastFrameX = ball.x
    local lastFrameY = ball.y
    local lastFrameTime = os.time()
    local xVelocity, frameTime
    ball.amIStillOnBoard = function()
        if ball.y < properties.y - ball.contentHeight then
            Runtime:dispatchEvent({ name = properties.eventTypeRoundOver, whoWon = "blue" })
            Runtime:removeEventListener("enterFrame", ball.amIStillOnBoard)
        elseif ball.y > properties.y + properties.height + ball.contentHeight then
            Runtime:dispatchEvent({ name = properties.eventTypeRoundOver, whoWon = "red" })
            Runtime:removeEventListener("enterFrame", ball.amIStillOnBoard)
        end

        xVelocity = ball.x - lastFrameX
        frameTime = os.time() - lastFrameTime

        lastFrameX = ball.x
        lastFrameY = ball.y
        lastFrameTime = os.time()
    end

    ball.getFrameXVelocityAndTime = function()
        log:debug("ball.getFrameXVelocityAndTime %s %s", xVelocity, frameTime)
        return xVelocity, frameTime -- returns one frame x difference and single frame time
    end

    ball.bounced = function()
        lastFrameX = ball.x
        lastFrameY = ball.y
        lastFrameTime = os.time()
        xVelocity = 0
        frameTime = 0
    end

    local function releaseBall()
        Runtime:addEventListener("enterFrame", ball.amIStillOnBoard)
    end

    Runtime:addEventListener(properties.eventTypeReleaseBall, releaseBall)

    function ball:finalize( event )
        Runtime:removeEventListener("enterFrame", ball.amIStillOnBoard)
        Runtime:removeEventListener(properties.eventTypeReleaseBall, releaseBall)
    end

    ball:addEventListener( "finalize" )

    ball.iAmBall = true -- for collisions detection
    return ball
end


return ballObject