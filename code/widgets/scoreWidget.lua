--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 16.06.2015
-- Time: 19:04
-- You deserve a coffee!
--

local tools = require("code.tools")
local properties = require("code.config.properties")

local scoreClass = {}

scoreClass.new = function(event)
--    local group = display.newGroup()

    local params = {
        score = event.score,
        time = event.time,
        delay = event.delay,
        callback = event.callback,
        anchorPointX = event.anchorPointX,
        anchorPointY = event.anchorPointY,
        fontSize = event.fontSize,
        startDelay = event.startDelay,
        startValue = event.startValue,
        color = event.color,
        prefix = event.prefix,
        suffix = event.suffix,
        sound = event.sound,
    }

    local scoreCounter = tools.scoreCounter(params)

    return scoreCounter
end


return scoreClass