--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 11.06.2015
-- Time: 21:49
-- You deserve a coffee!
--


local properties = require("code.config.properties")

local controllsObject = {}

controllsObject.new = function()
    local group = display.newGroup()


    -- top touch field

    local topTouchRect = display.newRect(properties.center.x, properties.y + 100, properties.width, 200)
    topTouchRect.alpha = 0.1
    group:insert(topTouchRect)


    local function topRectHandler(event)
        if event.phase == "began" then
            display.getCurrentStage():setFocus(event.target, event.id)
            event.target.topStartX = event.x
        elseif event.phase == "moved" then
            if not event.target.topStartX then event.target.topStartX = event.x end
            local movedX = event.x - event.target.topStartX
            event.target.topStartX = event.x
            Runtime:dispatchEvent({ name = properties.eventTypeMoveTopDisc, moveX = movedX })
        elseif event.phase == "ended" or event.phase == "cancelled" then
            event.target.topStartX = nil
            display.getCurrentStage():setFocus(event.target, nil)
        end

        return true
    end

    topTouchRect:addEventListener("touch", topRectHandler)


    -- bottom touch field

    local bottomTouchRect = display.newRect(properties.center.x, properties.y + properties.height - 100, properties.width, 200)
    bottomTouchRect.alpha = 0.1
    group:insert(bottomTouchRect)

    local function bottomRectHandler(event)
--        shallowLogTable(event, 2)
        if event.phase == "began" then
            display.getCurrentStage():setFocus(event.target, event.id)
            event.target.bottomStartX = event.x
        elseif event.phase == "moved" then
            if not event.target.bottomStartX then event.target.bottomStartX = event.x end
            local movedX = event.x - event.target.bottomStartX
            event.target.bottomStartX = event.x
            Runtime:dispatchEvent({ name = properties.eventTypeMoveBottomDisc, moveX = movedX })
        elseif event.phase == "ended" or event.phase == "cancelled" then
            event.target.bottomStartX = nil
            display.getCurrentStage():setFocus(event.target, nil)
        end

        return true
    end

    bottomTouchRect:addEventListener("touch", bottomRectHandler)


    -- center touch field

    local centerTouchRect = display.newRect(properties.center.x, properties.center.y , properties.width, properties.height - topTouchRect.contentHeight - bottomTouchRect.contentHeight)
    centerTouchRect.alpha = 0.01
    group:insert(centerTouchRect)

    local function centerRectHandler(event)
        if event.phase == "ended" then
            log:debug("centerRectHandler ended")
            Runtime:dispatchEvent({ name = properties.eventTypeReleaseBall })
        end
    end

    centerTouchRect:addEventListener("touch", centerRectHandler)

    return group
end


return controllsObject