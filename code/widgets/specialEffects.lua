--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 14.06.2015
-- Time: 11:28
-- You deserve a coffee!
--

local properties = require("code.config.properties")
local tools = require("code.tools")
local random = math.random

local effectsClass = {}

effectsClass.simpleText = function(event)
    local textValue = event.text
    local size = event.size
    local posX = event.x or properties.center.x
    local posY = event.y or properties.center.y
    local callback = event.callback
    local rotationsAmount = event.rotationsAmount or 0


    local textOptions =
    {
        text = textValue,
        x = posX,
        y = posY,
        width = properties.width, --required for multi-line and alignment
        font = properties.fontTypeSamovar,
        fontSize = size,
        align = "center"
    }

    local textObject = display.newText(textOptions)
    textObject:scale(0.0025, 0.0025)
    textObject.alpha = 0


    local function afterFadeOut()
        if textObject.fadeTransition then transition.cancel(textObject.fadeTransition); textObject.fadeTransition = nil end
        textObject:removeSelf()
        textObject = nil

        if callback then callback() end
    end

    local function fadeOut()
        if textObject.fadeTransition then transition.cancel(textObject.fadeTransition); textObject.fadeTransition = nil end
        textObject.fadeTransition = transition.to(textObject, {
            delay = 800,
            time = 500,
            alpha = 0,
            xScale = 0.0025,
            yScale = 0.0025,
            transition = easing.inBack,
            onComplete = afterFadeOut
        })
    end

    local function rotateIt()
        if textObject.fadeTransition then transition.cancel(textObject.fadeTransition); textObject.fadeTransition = nil end
        if rotationsAmount <= 0 then
            fadeOut()
            return
        end
        rotationsAmount = rotationsAmount - 1
        textObject.fadeTransition = transition.to(textObject, {
            delay = 800,
            time = 400,
            xScale = 0.1,
            yScale = 0.1,
            rotation = 180 * (rotationsAmount % 2 == 1 and 1 or -1),
            delta = true,
            transition = easing.inOutBack,
            onComplete = rotateIt
        })
    end


    local function fadeIn()
        textObject.fadeTransition = transition.to(textObject, {
            delay = 0,
            time = 500,
            alpha = 1,
            xScale = 1,
            yScale = 1,
            transition = easing.outBack,
            onComplete = rotationsAmount == 0 and fadeOut or rotateIt
        })
    end

    fadeIn()

    return textObject
end


effectsClass.tapToStartText = function(event)
    log:debug("effectsClass.tapToStartText")

    local noFlip = event and event.noFlip

    local removeMe, rotate, bounceOut, bounceIn, fadeIn

    local textOptions =
    {
        text = "Tap to start!",
        x = properties.center.x,
        y = properties.center.y,
        width = properties.width, --required for multi-line and alignment
        font = properties.fontTypeSamovar,
        fontSize = 54,
        align = "center"
    }

    local textObject = display.newText(textOptions)
    textObject:scale(0.0025, 0.0025)
    textObject.alpha = 0

    local bouncesNumber = 5

    removeMe = function()
        if textObject and textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        if textObject and textObject.removeSelf then textObject:removeSelf() end
    end

    textObject.close = function()
        if textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        textObject.textTransition = transition.to(textObject, {
            time = 500,
            alpha = 0.5,
            xScale = 0.0025,
            yScale = 0.0025,
            transition = easing.inBack,
            onComplete = removeMe
        })
    end

    rotate = function()
        if textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        textObject.textTransition = transition.to(textObject, {
            time = 800,
            rotation = 180,
            transition = easing.inOutBack,
            delta = true,
            onComplete = bounceOut
        })
    end

    bounceOut = function()
        if textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        if not noFlip then
            bouncesNumber = bouncesNumber - 1
        end
        if bouncesNumber <= 0 then
            rotate()
            bouncesNumber = 5
            return
        end
        textObject.textTransition = transition.to(textObject, {
            time = 800,
            alpha = 1,
            xScale = 1,
            yScale = 1,
            onComplete = bounceIn
        })
    end

    bounceIn = function()
        if textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        textObject.textTransition = transition.to(textObject, {
            time = 800,
            alpha = 0.7,
            xScale = 0.7,
            yScale = 0.7,
            onComplete = bounceOut
        })
    end

    fadeIn = function()
        if textObject.textTransition then transition.cancel(textObject.textTransition); textObject.textTransition = nil end
        textObject.textTransition = transition.to(textObject, {
            time = 600,
            alpha = 1,
            xScale = 1,
            yScale = 1,
            onComplete = bounceIn
        })
    end

    fadeIn()


    return textObject
end

effectsClass.burstSparks = function(event)
    local fromX = event.x
    local fromY = event.y
    local directionX = event.dirX
    local directionY = event.dirY
    local amount = event.amount or 6

    local burstGroup = display.newGroup()
    burstGroup.x = fromX
    burstGroup.y = fromY

    for i = 1, amount do
        local spark = tools.img("starSmall1.png", 128, 128)
        spark.x = random(-10, 10)
        spark.y = random(-10, 10)
        burstGroup:insert(spark)
        spark:scale(0.1, 0.1)
        spark.alpha = 0.2
        --        local sparkColor = properties.colorsTable[random(#properties.colorsTable)]
        --        spark:setFillColor(sparkColor[1], sparkColor[2], sparkColor[3], 0.5)

        local function finishTransition()
            if spark.burstTransition then transition.cancel(spark.burstTransition); spark.burstTransition = nil end
            spark:removeSelf()
            spark = nil
        end

        local xMax = directionX and 280 or 45
        local yMax = directionY and 280 or 45
        local xMin = directionX and 80 or 1
        local yMin = directionY and 80 or 1
        local distanceX = random(xMin, xMax) * (directionX or random(2) == 1 and 1 or -1)
        local distanceY = random(yMin, yMax) * (directionY or random(2) == 1 and 1 or -1)
        spark.burstTransition = transition.to(spark, {
            time = 400,
            x = distanceX,
            y = distanceY,
            xScale = random(2, 7) * 0.1,
            yScale = random(2, 7) * 0.1,
            alpha = random(2, 7) * 0.1,
            rotation = random(-45, 45),
            onComplete = finishTransition
        })
    end

    return burstGroup
end


return effectsClass