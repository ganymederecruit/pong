--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 09.08.14
-- Time: 12:07
-- To change this template use File | Settings | File Templates.
--


local composer = require("composer")
local properties = require("code.config.properties")
local tools = require("code.tools")
local tnt = require("code.tools.transitionsManagement")
local creditsClass = composer.newScene()


function creditsClass:create(event)
    local creditsScreen = self.view

    local p = {
        banners = {
            { name = "artifex-splash.jpg", width = 420, height = 408 },
            { name = "ui_logo_pong.png", width = 340, height = 200 },
        }
    }


    local function startGame()
        composer.gotoScene("code.startScreen")
        composer.removeScene("code.credits")
    end

    local group = display.newGroup()

    local function animateBanner(number, finish)
        log:debug("animateBanner")
        local bannerImg = tools.img(p.banners[number].name, p.banners[number].width, p.banners[number].height)
        bannerImg.x = properties.center.x
        bannerImg.y = properties.center.y
        group:insert(bannerImg)
        bannerImg.alpha = 0

        local function bannerRemove()
            if bannerImg.transition then transition.cancel(bannerImg.transition); bannerImg.transition = nil end

            bannerImg:removeSelf()
            bannerImg = nil

            if finish then startGame() end
        end

        local function bannerHide()
            if bannerImg.transition then transition.cancel(bannerImg.transition); bannerImg.transition = nil end

            bannerImg.transition = transition.to(bannerImg, { time = properties.bannerTransitionTime, delay = properties.bannerStayOnTime, alpha = 0, onComplete = bannerRemove })
        end

        bannerImg.transition = transition.to(bannerImg, { time = properties.bannerTransitionTime, alpha = 1, onComplete = bannerHide })
    end

    for i = 1, #p.banners do
        tnt:newTimer(properties.bannerShuffleDelay * (i - 1), function()
            animateBanner(i, i == #p.banners)
        end)
    end

    creditsScreen:insert(group)
end

-- "creditsClass:show()"
function creditsClass:show(event)

    local boardGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Called when the board is still off screen (but is about to come on screen).
    elseif (phase == "did") then
        -- Called when the board is now on screen.
        -- Insert code here to make the board come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end


-- "creditsClass:hide()"
function creditsClass:hide(event)

    local boardGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Called when the board is on screen (but is about to go off screen).
        -- Insert code here to "pause" the board.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif (phase == "did") then
        -- Called immediately after board goes off screen.
    end
end


-- "creditsClass:destroy()"
function creditsClass:destroy(event)

    local boardGroup = self.view

    -- Called prior to the removal of board's view ("boardGroup").
    -- Insert code here to clean up the board.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
creditsClass:addEventListener("create", creditsClass)
creditsClass:addEventListener("show", creditsClass)
creditsClass:addEventListener("hide", creditsClass)
creditsClass:addEventListener("destroy", creditsClass)

-- -------------------------------------------------------------------------------

return creditsClass