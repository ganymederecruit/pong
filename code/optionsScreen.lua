--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 15.06.2015
-- Time: 13:46
-- You deserve a coffee!
--

local composer = require("composer")
local properties = require("code.config.properties")
local buttonClass = require("code.button")
local tools = require("code.tools")
local options = require("code.config.options")
local optionsScreenClass = composer.newScene()




function optionsScreenClass:create(event)
    local screenGroup = self.view

    local group = display.newGroup()
    screenGroup:insert(group)

    local p = {
        sound = options.sound,
        music = options.music,
        difficulty = options.difficulty,
    }
    local buttonsTable = {}
    local difficultyTxt

    local menuBG = tools.img("menu-background.jpg", 640, 960)
    menuBG.x = properties.center.x
    menuBG.y = properties.center.y
    group:insert(menuBG)
    if menuBG.contentWidth < properties.width or menuBG.contentHeight < properties.height then
        local xScale = properties.width / menuBG.contentWidth
        local yScale = properties.height / menuBG.contentHeight
        local maxScale = xScale > yScale and xScale or yScale
        menuBG:scale(maxScale, maxScale)
    end


    -- buttons

    local buttonsDefTable

    local function callback1(event)
        log:debug("options callback1 difficulty")

        p.difficulty = p.difficulty + 1
        if p.difficulty > #properties.difficulty then p.difficulty = 1 end

        options.save("difficulty", p.difficulty)
        options.store()

        difficultyTxt.text = properties.difficulty[p.difficulty].name
    end

    local function callback2(event)
        log:debug("options callback2 sound")

        p.sound = not p.sound
        options.save("sound", p.sound)
        options.store()

        buttonsTable[2].textObject.text = "Sound " .. (p.sound and "on" or "off")
    end

    local function callback3(event)
        log:debug("options callback3 music")

        p.music = not p.music
        options.save("music", p.music)
        options.store()

        if p.music then
            Runtime:dispatchEvent({ name = properties.playAudioMusic })
        else
            Runtime:dispatchEvent({ name = properties.stopAudioMusic })
        end

        buttonsTable[3].textObject.text = "Music " .. (p.music and "on" or "off")
    end

    local function callback4(event)
        log:debug("options callback4 return")

        composer.gotoScene("code.startScreen")
        composer.removeScene("code.optionsScreen")
    end

    buttonsDefTable = {
        { name = "Difficulty level", callback = callback1, x = properties.center.x - properties.width * 0.0, y = properties.y + properties.height * 0.2 },
        { name = "Sound " .. (p.sound and "on" or "off"), callback = callback2, x = properties.center.x - properties.width * 0.0, y = properties.y + properties.height * 0.45 },
        { name = "Music " .. (p.music and "on" or "off"), callback = callback3, x = properties.center.x - properties.width * 0.0, y = properties.y + properties.height * 0.58 },
        { name = "Return", callback = callback4, x = properties.center.x - properties.width * 0.15, y = properties.y + properties.height * 0.9 },
    }


    for i = 1, #buttonsDefTable do
        local buttonImg = tools.img("button.png", 389, 99)
        local buttonTxt = display.newText(buttonsDefTable[i].name, 0, 0, properties.fontTypeSamovar, 36)
        local localButton = buttonClass.simple({ image = buttonImg, addon = buttonTxt, addonShiftY = -4, callback = buttonsDefTable[i].callback })
        localButton.x = buttonsDefTable[i].x
        localButton.y = buttonsDefTable[i].y
        group:insert(localButton)
        localButton.textObject = buttonTxt
        table.insert(buttonsTable, localButton)
    end

    difficultyTxt = display.newText(properties.difficulty[p.difficulty].name, 0, 0, properties.fontTypeSamovar, 32)
    difficultyTxt.x = buttonsTable[1].x
    difficultyTxt.y = buttonsTable[1].y + 90
    group:insert(difficultyTxt)
end

function optionsScreenClass:show(event)

    local screenGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end


function optionsScreenClass:hide(event)

    local screenGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end


function optionsScreenClass:destroy(event)

    local screenGroup = self.view
end


-- -------------------------------------------------------------------------------

-- Listener setup
optionsScreenClass:addEventListener("create", optionsScreenClass)
optionsScreenClass:addEventListener("show", optionsScreenClass)
optionsScreenClass:addEventListener("hide", optionsScreenClass)
optionsScreenClass:addEventListener("destroy", optionsScreenClass)

-- -------------------------------------------------------------------------------

return optionsScreenClass