--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 10.08.14
-- Time: 01:45
-- You deserve a coffee!
--


local properties = require("code.config.properties")
local options = require("code.config.options")

local audioSystemObject = {}

local sounds = {}

audioSystemObject.new = function()

    audio.reserveChannels( 2 )

    sounds.bounce1 = audio.loadSound( "sounds/bounce1.mp3" )
    sounds.bounce2 = audio.loadSound( "sounds/bounce2.mp3" )
    sounds.success = audio.loadSound( "sounds/crowdGood.mp3" )
    sounds.failure = audio.loadSound( "sounds/crowdBoo.mp3" )

    sounds.music = audio.loadStream( "sounds/introMusic.mp3" )

    audio.setVolume( 0.5 )

    local playBounce1 = function()
        if not options.sound then return end
        audio.play( sounds.bounce1 )
    end

    local playButton = function()
        if not options.sound then return end
        audio.play( sounds.bounce2 )
    end

    local playSuccess = function()
        if not options.sound then return end
        audio.play( sounds.success )
    end

    local playFailure = function()
        if not options.sound then return end
        audio.play( sounds.failure )
    end




    local playMusic = function()
        log:debug("playMusic %s", tostring(options.music))
        if not options.music then return end

        audio.setVolume( 0.5, { channel=1 } )
        audio.play( sounds.music, { channel = 1, fadein = 5000, loops = -1 } )

    end

    local stopMusic = function(params)
        local time = params and params.time or 2000
        audio.fadeOut( { channel = 1, time = time } )
    end

    Runtime:addEventListener( properties.playAudioBounce1, playBounce1)
    Runtime:addEventListener( properties.eventTypeButtonPressed, playButton)
    Runtime:addEventListener( properties.playAudioSuccess, playSuccess)
    Runtime:addEventListener( properties.playAudioFailure, playFailure)
    Runtime:addEventListener( properties.playAudioMusic, playMusic)
    Runtime:addEventListener( properties.stopAudioMusic, stopMusic)

end

return audioSystemObject