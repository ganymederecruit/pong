--
-- Created by IntelliJ IDEA.
-- User: krystian
-- Date: 6/21/12
-- Time: 8:15 PM
--
E = "HiI'manewenemyyou'llfightinthecominglevels.Irunfastandburstout.Preparetodie"
--local openssl=require("plugin.openssl")
local cr = require("crypto")
--for k,v in pairs(openssl) do print(k,v) end
local m = require("mime")
local c = {}
local ch = cr.hmac
local c5 = cr.sha512
--local oc=openssl.get_cipher("aes-256-cbc")
local mb = mime.b64
local mub = mime.unb64

local function k()
    return string.sub(E, 17 * 2 + 5 % 4 - 4, 20 * 2 + 5) .. string.sub(E, 3 % 2, (12 * 3 - 6) * 0.5) .. string.sub(E, 12 * 4 - 2, 2 ^ 2 * 5 * 4) .. string.sub(E, 8 ^ 2 + 4 % 3 - 2 ^ 2, 9 ^ 2 - 7) .. string.sub(E, 8 * 4 / 2, 10 * 3)
end

local function gk()
    return cr.digest(c5, k())
end

c.ae = function(value)
    return ch(c5, k(), value)
end

c.ac = function(e, v)
    return ch(c5, k(), v) == e
end

c.ec = function(value)
    return mb(oc:encrypt(value, gk()))
end

c.dc = function(value)
    return mub(oc:decrypt(value, gk()))
end

return c
