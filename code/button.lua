local tools = require("code.tools")
local properties = require("code.config.properties")
local _Button = {}
_Button.currentButton = nil
_Button.touchEnabled = true

_Button.toggle = function(event)
    log:debug("Button touch enabled: %s", tostring(event.state))
    _Button.touchEnabled = event.state
end

_Button.simple = function(params)
    local buttonGroup = display.newGroup()

    local enabled = true
    local noAnimation = params.noAnimation
    local imgType = 1

    local button
    if type(params.image) == "table" then
        imgType = 2
        button = params.image
    else
        button = tools.img(params.image)
    end

    buttonGroup:insert(button)
    if params.scale then button:scale(params.scale, params.scale) end
    --    button.x = 0
    --    button.y = 0

    if params.object then
        params.object.x = button.x + (params.objectShiftX or 0)
        params.object.y = button.y + (params.objectShiftY or 0)
        buttonGroup:insert(params.object)
    end

    if params.addon then
        local addon = params.addon
        addon.x = button.x + (params.addonShiftX or 0)
        addon.y = button.y + (params.addonShiftY or 0)
        buttonGroup:insert(addon)
    end

    if params.rgbClick then
        button.rgbClick = params.rgbClick
    end

    if params.rgbUnClick then
        button.rgbUnClick = params.rgbUnClick
    end
    buttonGroup.callback = params.callback

    local dim = function()
        if button.rgbClick then
            button:setFillColor(button.rgbClick[1] / 255, button.rgbClick[2] / 255, button.rgbClick[3] / 255, button.rgbClick[4])
        end
        if not params.noAnim then
            if buttonGroup.transition then transition.cancel(buttonGroup.transition); buttonGroup.transition = nil end
            buttonGroup.transition = transition.to(buttonGroup, { time = 500,  xScale = 1.2, yScale = 1.2, transition = easing.outElastic })
        end

    end

    local undim = function()
        if button.rgbUnClick then
            button:setFillColor(button.rgbUnClick[1] / 255, button.rgbUnClick[2] / 255, button.rgbUnClick[3] / 255, button.rgbUnClick[4])
        end
        if not params.noAnim then
            if buttonGroup.transition then transition.cancel(buttonGroup.transition); buttonGroup.transition = nil end
            buttonGroup.transition = transition.to(buttonGroup, { time = 500, xScale = 1, yScale = 1, transition = easing.outElastic })
        end
    end

    --DEV
    params.noSound = true

    local isFocused = false
    local function isWithinBounds(event)
        local bounds = button.contentBounds
        local x, y = event.x, event.y
        return bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
    end

    local function onEventFunction(event)
        if not enabled or not _Button.touchEnabled then return end
        if event.phase == "began" then
            if not _Button.currentButton then
                _Button.currentButton = buttonGroup
                display:getCurrentStage():setFocus(button, event.id)
                isFocused = true
                dim()
            end
        elseif event.phase == "moved" then
            if not isWithinBounds(event) and tostring(_Button.currentButton) == tostring(buttonGroup) then
                _Button.currentButton = nil
                undim()
                display:getCurrentStage():setFocus(button, nil)
                isFocused = false
                return false
            end
        elseif event.phase == "ended" then
            if tostring(_Button.currentButton) == tostring(buttonGroup) then
                local cb = buttonGroup.callback
                undim()
                if cb and (not params.checker or (params.checker and params.checker())) then
                    _Button.currentButton = nil
                    log:info("Button click END")
                    cb()
                    Runtime:dispatchEvent({ name = properties.eventTypeButtonPressed })
                else
                    _Button.currentButton = nil
                end
                isFocused = false
                display:getCurrentStage():setFocus(button, nil)
            end

        else
            undim()
            isFocused = false
            _Button.currentButton = nil
            display:getCurrentStage():setFocus(button, nil)
        end
        return true
    end

    button:addEventListener("touch", onEventFunction)

    if params.buttonScaleX and params.buttonScaleY then
        buttonGroup.xScale = params.buttonScaleX
        buttonGroup.yScale = params.buttonScaleY
    end

    function buttonGroup:activate()
        log:info("Button activated")
        enabled = true
    end

    function buttonGroup:isActivated()
        return enabled
    end

    function buttonGroup:deactivate()
        log:info("Button deactivated")
        enabled = false
    end

    function buttonGroup:rebuild()
        isFocused = false
        if button and button.removeSelf then display:getCurrentStage():setFocus(button, nil) end
    end

    function buttonGroup:setFocus(event)
        display.getCurrentStage():setFocus(button)
        onEventFunction(event)
    end

    return buttonGroup
end

Runtime:addEventListener(properties.eventTypeToggleButton, _Button.toggle)

return _Button