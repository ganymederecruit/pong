--
-- Created by IntelliJ IDEA.
-- User: maciej
-- Date: 14.10.2013
-- Time: 09:05
--

local properties = require("code.config.properties")
local tnt = require("code.tools.transitionsManagement")

local CONST_A = 0.25 * math.sqrt(3.0)
local NUM_DIRECTIONS = 6

local gsub = string.gsub
local floor = math.floor
local ceil = math.ceil
local tools = {}

--tools.staticImageSheet = require("code.imagesheets.imagesheet_static")
--tools.staticImageSheet.img = graphics.newImageSheet("graphics/imagesheets/imagesheet_static.png", tools.staticImageSheet.spriteInfo)

tools.imgInfo = function(name)
    local f = tools.staticImageSheet.frameInfo[name]
    return f, tools.staticImageSheet.spriteInfo.frames[f] --zwracam 2 wartosci
end

tools.img = function(name, width, height)
    local dobj
    if width == nil then
        local frame -- = tools.staticImageSheet.frameInfo[name]
        if not frame then log:error("Frame not found for name: %s", tostring(name)) tools.traceback() end
        dobj = display.newImage(tools.staticImageSheet.img, frame)
    else
        dobj = display.newImageRect("graphics/" .. name, width, height)
    end
    return dobj
end

tools.commaValue = function(amount)
    local formatted, k = amount, nil

    while true do
        formatted, k = gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
        if (k == 0) then
            break
        end
    end
    return formatted
end

tools.levelTimeString = function(time)
    local sec = time % 60
    if sec < 10 then sec = "0" .. tostring(sec) end
    local minutes = math.floor(time / 60)

    return tostring(minutes) .. ":" .. tostring(sec)
end

tools.alphabet = function(params)
    local lg = display.newGroup()
    local g2Wrapper = tools.newG2Wrapper()
    g2Wrapper:insert(lg)
    local origText = params.text
    local words = tools.explode(" ", origText)
    local x = params.x or 0
    local y = params.y or 0
    local scaleX = params.scaleX or 1
    local scaleY = params.scaleY or 1
    local lastX = 0
    local fontSheet = params.fontSheet

    local function checkUnicode(char)
        local Byte0 = string.byte(char, 1);
        if (floor(Byte0 / 0x80) == 0) then return true; end;
    end


    for i = 1, #words do
        local word = words[i]
        local j = 1
        while j <= word:len() do
            local letter = word:sub(j, j)
            if not checkUnicode(letter) then
                letter = word:sub(j, j + 1)
                j = j + 1
            end
            local bmpLetter = tools.font(letter, fontSheet)
            bmpLetter:scale(scaleX, scaleY)
            if not bmpLetter then
                letter = tools.CodeToUTF8(tools.CodeFromUTF8(letter))
                bmpLetter = tools.font(letter, fontSheet)
            end

            lg:insert(bmpLetter)
            bmpLetter.anchorX = 0
            bmpLetter.anchorY = 0.5
            bmpLetter.x = x
            bmpLetter.y = y
            x = x + bmpLetter.contentWidth - (params.overlap or 4)
            j = j + 1
        end
        x = x + 10
    end

    return g2Wrapper
end

tools.scoreCounter = function(params)
    local score = params.score
    local time = params.time
    local delay = params.delay
    local callback = params.onComplete
    local anchorPointX = params.anchorPointX or 0.5
    local anchorPointY = params.anchorPointY or 0.5
    local fontSize = params.fontSize or 24
    local startDelay = params.startDelay or 1000
    local startValue = params.startValue or 0
    local color = params.color or { r = 1, g = 1, b = 1 }
    local prefix = params.prefix or ""
    local suffix = params.suffix or ""
    local sound = params.sound

    local startTimer, addTimer

    local text = display.newText(prefix .. tools.commaValue(startValue) .. suffix, 0, 0, properties.fontTypeCounter, fontSize)
    text:setFillColor(color[1], color[2], color[3])

    local count = floor(time / delay)
    local delta = floor(score / count)
    local modulo = startValue + score - delta * count
    local current = startValue

    local function changeText(what, timerCount)
        if sound then
            Runtime:dispatchEvent({ name = properties.eventTypeCounterSound })
        end
        text.text = prefix .. tools.commaValue(tostring(what)) .. suffix
        text:setFillColor(color[1], color[2], color[3])
        text.anchorX = anchorPointX
        text.anchorY = anchorPointY
--        text.x = params.x
--        text.y = params.y + 4
        if timerCount and timerCount == count - 2 then if callback then callback() end end
    end

    local function add(event)
        current = current + delta
        changeText(tostring(current), event.count)
    end

    local function start()
        current = delta + modulo
        changeText(current)
        addTimer = tnt:newTimer(delay, add, count - 1, "scoreCounter")
    end

    changeText(startValue)

    startTimer = tnt:newTimer(startDelay, start, 1, "scoreCounter")

    text.removeMe = function()
        if addTimer then
            addTimer:cancel()
            addTimer = nil
        end

        if startTimer then
            startTimer:cancel()
            startTimer = nil
        end

        text:removeSelf()
        text = nil
    end

    return text
end

tools.scoreCounterAlphabet = function(params)
    local grp = display.newGroup()
    --    grp.anchorChildren = true
    local score = params.score
    local time = params.time
    local delay = params.delay
    local callback = params.onComplete
    local refPointX = params.refPointX or 0.5
    local refPointY = params.refPointY or 0.5
    local fontSize = params.fontSize or 24
    local startDelay = params.startDelay or 1000
    local startValue = params.startValue or 0
    local suffix = params.suffix or ""
    local fontSheet = params.fontSheet or tools.darkSmallFontSheet
    local scale = params.scale
    local maxW = params.maxW

    local textField = tools.alphabet({ text = tostring(startValue) .. suffix, fontSheet = fontSheet }) --display.newText(math.round(progress * 100) .. "%", 0, 0, properties.font, 14)
    grp:insert(textField)

    local count = floor(time / delay)
    local delta = floor(score / count)
    local modulo = startValue + score - delta * count
    local current = startValue

    local startTimer, addTimer

    local function changeText(what, timerCount)

        if textField then textField:removeSelf() textField = nil end
        textField = tools.alphabet({ text = what .. suffix, fontSheet = fontSheet })
        if scale then
            textField:scale(scale, scale)
            if maxW and textField.contentWidth > maxW then
                local s = maxW / textField.contentWidth
                textField:scale(s, s)
            end
        end

        textField.anchorX = refPointX
        textField.anchorY = refPointY
        textField.x = params.x
        textField.y = params.y
        grp:insert(textField)

        if timerCount and timerCount == count - 2 then if callback then callback() end end
    end

    local function add(event)

        current = current + delta
        --        log:info("current %s", current)
        changeText(tostring(current), event.count)
    end

    local function start()
        current = delta + modulo
        changeText(current)
        addTimer = tnt:newTimer(delay, add, count - 1, "scoreCounter")
    end

    changeText(startValue)

    startTimer = tnt:newTimer(startDelay, start, 1, "scoreCounter")

    grp.removeMe = function()
        if addTimer then
            addTimer:cancel()
            addTimer = nil
        end

        if startTimer then
            startTimer:cancel()
            startTimer = nil
        end

        textField:removeSelf()
        textField = nil
    end

    return grp
end

tools.explode = function(div, str)
    if (div == '') then return false end
    local pos, arr = 0, {}
    -- for each divider found
    for st, sp in function() return string.find(str, div, pos, true) end do
        -- Attach chars left of current divider
        table.insert(arr, string.sub(str, pos, st - 1))
        pos = sp + 1 -- Jump past current divider
    end
    -- Attach chars right of last divider
    table.insert(arr, string.sub(str, pos))
    return arr
end

tools.multiline = function(params)
    local lg = display.newGroup()
    --    lg.anchorChildren = true
    local textArr = tools.explode(" ", params.desc)

    local description2 = display.newText(textArr[1], 0, 0, params.font, params.fontSize)
    local temp = display.newText("", 0, 0, params.font, params.fontSize)
    local textColor = params.color or { r = 0.396, g = 0, b = 0.011 }
    local shiftY = description2.contentHeight * 0.5 + 8

    lg:insert(description2)

    description2:setFillColor(textColor.r, textColor.g, textColor.b)
    description2.anchorX = params.referencePointX
    description2.anchorY = params.referencePointY
    description2.y = params.y
    description2.x = params.x

    local lineCount = 0

    if textArr[2] then
        for i = 2, #textArr do
            local _txt = textArr[i]
            temp.text = description2.text .. " " .. _txt
            if temp.contentWidth < params.maxWidth then
                description2.text = description2.text .. " " .. _txt
                description2.anchorX = params.referencePointX
                description2.anchorY = params.referencePointY
                description2.x = params.x
            else
                lineCount = lineCount + 1
                description2 = display.newText(_txt, 0, 0, params.font, params.fontSize)
                description2:setFillColor(textColor.r, textColor.g, textColor.b)
                lg:insert(description2)
                description2.anchorX = params.referencePointX
                description2.anchorY = params.referencePointY
                description2.y = params.y + shiftY * lineCount
                description2.x = params.x
            end
        end
    end
    temp:removeSelf()
    temp = nil

    return lg
end

tools.newText = function(text, pX, pY, pWidth, pHeight, pFont, pMaxSize, pColor)
    local x, y, width, height, font, maxSize, color
    if pMaxSize == nil then
        x = 0
        y = 0
        width = pX
        height = pY
        font = pWidth
        maxSize = pHeight
        color = pFont
    else
        x = pX
        y = pY
        width = pWidth
        height = pHeight
        font = pFont
        maxSize = pMaxSize
        color = pColor
    end
    if not color then
        color = { 1, 1, 1 }
    elseif type(color) == "number" then
        color = { color, color, color }
    elseif type(color) == "table" and #color == 1 then
        color = { color[1], color[1], color[1] }
    end

    local txt = display.newText(text, 0, 0, width, 0, font, maxSize)
    local i = 0
    local ok = false
    while (not ok) do
        ok = true
        local tSize = txt.size
        local tWidth = txt.contentWidth
        for i, v in ipairs(tools.explode(" ", text)) do
            local explodeTest = display.newText(v, 0, 0, font, tSize)
            if explodeTest.contentWidth > tWidth then ok = false explodeTest:removeSelf() explodeTest = nil break else explodeTest:removeSelf() explodeTest = nil end
        end
        if ok then ok = txt.contentHeight <= height end
        if not ok then
            i = i + 1
            txt.size = maxSize - i
        end
    end
    --check for short single line
    local tstTxt = display.newText(text, 0, 0, font, txt.size)
    if (tstTxt.contentWidth < width) then
        tstTxt:setFillColor(color[1], color[2], color[3])
        txt:removeSelf()
        txt = tstTxt
        tstTxt = nil
    else
        -- check if words were not cut in the middle
        local tSize = txt.size
        local tWidth = txt.contentWidth
        tstTxt:removeSelf()
        tstTxt = nil
        txt:removeSelf()
        txt = tools.multiline({ font = font, fontSize = tSize, desc = text, color = { r = color[1], g = color[2], b = color[3] }, maxWidth = tWidth, x = 0, y = 0, referencePointX = 0.5, referencePointY = 0.5 })
    end
    txt.anchorX = 0
    txt.anchorY = 0
    txt.x = x
    txt.y = y
    txt.anchorX = 0.5
    txt.anchorY = 0.5
    return txt
end

tools.traceback = function()
--do return end
    local level = 1
    while true do
        local info = debug.getinfo(level, "Sl")
        if not info then break end
        if info.what == "C" then -- is a C function?
            print(level, "C function")
        else -- a Lua function
            print(string.format("[%s]:%d",
                info.short_src, info.currentline))
        end
        level = level + 1
    end
end

tools.CodeFromUTF8 = function(UTF8)
    local Byte0 = string.byte(UTF8, 1);
    if (floor(Byte0 / 0x80) == 0) then return Byte0; end;

    local Byte1 = string.byte(UTF8, 2) % 0x40;
    if (floor(Byte0 / 0x20) == 0x06) then
        return (Byte0 % 0x20) * 0x40 + Byte1;
    end;

    local Byte2 = string.byte(UTF8, 3) % 0x40;
    if (floor(Byte0 / 0x10) == 0x0E) then
        return (Byte0 % 0x10) * 0x1000 + Byte1 * 0x40 + Byte2;
    end;

    local Byte3 = string.byte(UTF8, 4) % 0x40;
    if (floor(Byte0 / 0x08) == 0x1E) then
        return (Byte0 % 0x08) * 0x40000 + Byte1 * 0x1000 + Byte2 * 0x40 + Byte3;
    end;
end

tools.CodeToUTF8 = function(Unicode)
    if (Unicode <= 0x7F) then return string.char(Unicode); end;

    if (Unicode <= 0x7FF) then
        local Byte0 = 0xC0 + floor(Unicode / 0x40);
        local Byte1 = 0x80 + (Unicode % 0x40);
        return string.char(Byte0, Byte1);
    end;

    if (Unicode <= 0xFFFF) then
        local Byte0 = 0xE0 + floor(Unicode / 0x1000);
        local Byte1 = 0x80 + (floor(Unicode / 0x40) % 0x40);
        local Byte2 = 0x80 + (Unicode % 0x40);
        return string.char(Byte0, Byte1, Byte2);
    end;

    return ""; -- ignore UTF-32 for the moment
end

tools.camelCase = function(str)
    local result = ''
    for word in string.gmatch(str, "%S+") do
        local first = string.sub(word, 1, 1)
        result = (result .. string.upper(first) ..
                string.lower(string.sub(word, 2)) .. ' ')
    end
    return result
end

tools.fillZero = function(date)
    if date < 10 then return "0" .. date
    else return date
    end
end

tools.newTextConstrained = function(text, width, font, size)
    local txt = display.newText(text, 0, 0, font, size)
    local ok = false
    local pass = 1
    while (not ok) do
        local tWidth = txt.contentWidth
        if tWidth > width then
            txt:removeSelf()
            txt = nil
            local currentText = string.sub(text, 1, string.len(text) - pass) .. "..."
            pass = pass + 1
            txt = display.newText(currentText, 0, 0, font, size)
        else
            ok = true
        end
        if pass >= string.len(text) - 1 then -- screwup
            ok = true
        end
    end

    return txt
end

tools.newTextWithinBounds = function(text, pWidth, pFont, pMaxSize, pMinSize)
    local width, font, maxSize, minSize

    width = pWidth
    font = pFont
    maxSize = pMaxSize
    if pMinSize then minSize = pMinSize else minSize = 8 end

    local txt = display.newText(text, 0, 0, font, maxSize)
    local i = 0
    local ok = false
    local cut = false
    while (not ok) do
        if txt.contentWidth <= pWidth then
            ok = true
        else
            i = i + 1
            txt.size = maxSize - i
            if txt.size < pMinSize then
                ok = true
                cut = true
            end
        end
    end
    if cut then
        local currentSize = txt.size
        txt:removeSelf()
        txt = nil
        txt = tools.newTextConstrained(text, pWidth, font, currentSize)
    end

    return txt
end

--local sysFonts = native.getFontNames()
--for k,v in pairs(sysFonts) do log:debug(v) end

return tools