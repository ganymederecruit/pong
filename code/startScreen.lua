--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 11.06.2015
-- Time: 20:01
-- You deserve a coffee!
--




local composer = require("composer")
local properties = require("code.config.properties")
local buttonClass = require("code.button")
local tools = require("code.tools")
local startScreenClass = composer.newScene()




function startScreenClass:create(event)
    local startScreen = self.view


    local menuBG = tools.img("menu-background.jpg", 640, 960)
    menuBG.x = properties.center.x
    menuBG.y = properties.center.y
    startScreen:insert(menuBG)
    if menuBG.contentWidth < properties.width or menuBG.contentHeight < properties.height then
        local xScale = properties.width / menuBG.contentWidth
        local yScale = properties.height / menuBG.contentHeight
        local maxScale = xScale > yScale and xScale or yScale
        menuBG:scale(maxScale, maxScale)
    end


    local gameLogoImg = tools.img("ui_logo_pong.png", 170, 100)
    gameLogoImg.x = properties.center.x
    gameLogoImg.y = properties.center.y - properties.height * 0.3
    startScreen:insert(gameLogoImg)


    local producerLogoImg = tools.img("artifex-logo-small.png", 190, 185)
    producerLogoImg.x = properties.center.x + properties.width * 0.32
    producerLogoImg.y = properties.center.y + properties.height * 0.40
    startScreen:insert(producerLogoImg)


    local function startSingleGame()
        log:debug("startSingleGame!")
        Runtime:dispatchEvent({ name = properties.stopAudioMusic })
        composer.gotoScene("code.board", { params = { gameType = "single" }})
        composer.removeScene( "code.startScreen")
    end

    local function startHotSeatGame()
        log:debug("startHotSeatGame!")
        Runtime:dispatchEvent({ name = properties.stopAudioMusic })
        composer.gotoScene("code.board", { params = { gameType = "hotSeat" }})
        composer.removeScene( "code.startScreen")
    end

    local function openOptions()
        log:debug("openOptions!")
        composer.gotoScene("code.optionsScreen", { params = {  }})
        composer.removeScene( "code.startScreen")
    end



    local startSingleGameRect = tools.img("button.png", 450, 99)
    local startSingleGameText = display.newText("Single Game", 0, 0, properties.fontTypeSamovar, 36)
    local startSingleGameButton = buttonClass.simple({ callback = startSingleGame, image = startSingleGameRect, addon = startSingleGameText, addonShiftY = -4 })
    startSingleGameButton.x = properties.center.x --+ startSingleGameButton.contentWidth * 0.55
    startSingleGameButton.y = properties.center.y - 100

    startScreen:insert(startSingleGameButton)


    local startHotseatGameRect = tools.img("button.png", 450, 99)
    local startHotseatGameText = display.newText("Hot Seat Game", 0, 0, properties.fontTypeSamovar, 36)
    local startHotseatGameButton = buttonClass.simple({callback = startHotSeatGame, image = startHotseatGameRect, addon = startHotseatGameText, addonShiftY = -4})
    startHotseatGameButton.x = properties.center.x --+ startHotseatGameButton.contentWidth * 0.55
    startHotseatGameButton.y = properties.center.y + 50

    startScreen:insert(startHotseatGameButton)


    local optionsRect = tools.img("button.png", 450, 99)
    local optionsText = display.newText("Options", 0, 0, properties.fontTypeSamovar, 36)
    local optionsButton = buttonClass.simple({callback = openOptions, image = optionsRect, addon = optionsText, addonShiftY = -4})
    optionsButton.x = properties.center.x-- + optionsButton.contentWidth * 0.55
    optionsButton.y = properties.center.y + 200

    startScreen:insert(optionsButton)


end

-- "startScreenClass:show()"
function startScreenClass:show(event)

    local boardGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Called when the board is still off screen (but is about to come on screen).
    elseif (phase == "did") then
        -- Called when the board is now on screen.
        -- Insert code here to make the board come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end


-- "startScreenClass:hide()"
function startScreenClass:hide(event)

    local boardGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Called when the board is on screen (but is about to go off screen).
        -- Insert code here to "pause" the board.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif (phase == "did") then
        -- Called immediately after board goes off screen.
    end
end


-- "startScreenClass:destroy()"
function startScreenClass:destroy(event)

    local boardGroup = self.view

    -- Called prior to the removal of board's view ("boardGroup").
    -- Insert code here to clean up the board.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
startScreenClass:addEventListener("create", startScreenClass)
startScreenClass:addEventListener("show", startScreenClass)
startScreenClass:addEventListener("hide", startScreenClass)
startScreenClass:addEventListener("destroy", startScreenClass)

-- -------------------------------------------------------------------------------

return startScreenClass