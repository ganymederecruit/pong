--
-- Created by IntelliJ IDEA.
-- User: lukis
-- Date: 11.06.2015
-- Time: 20:27
-- You deserve a coffee!
--



local composer = require("composer")
local properties = require("code.config.properties")
local button = require("code.button")
local tnt = require("code.tools.transitionsManagement")
local tools = require("code.tools")
local ballClass = require("code.widgets.ball")
local controlsClass = require("code.widgets.controls")
local specialEffects = require("code.widgets.specialEffects")
local scoreWidget = require("code.widgets.scoreWidget")
local options = require("code.config.options")
local physics = require "physics"

local board = composer.newScene()

local floor = math.floor
local ceil = math.ceil
local round = math.round
local random = math.random



function board:create(event)
    local scene = self.view

    local p = {}
    local tapToStart
    local ball
    local bottomScore, topScore

    p.difficultyLevel = properties.difficulty[options.difficulty].deltaT

    local group = display.newGroup()
    scene:insert(group)

    -- background

    local function generateBackground()
        local tileDimensions = { width = 256, height = 256 }
        local bgGroup = display.newGroup()

        local bgGrassGroup = display.newGroup()
        bgGrassGroup.x = properties.center.x
        bgGrassGroup.y = properties.center.y

        -- generate pitch
        local tileCols = ceil(properties.width / tileDimensions.width)
        local tileRows = ceil(properties.height / tileDimensions.height)

        local tileGroupCenter = { x = tileDimensions.width * tileCols * 0.5, y = tileDimensions.height * tileRows * 0.5 }

        log:debug("background dimensions tileCols %s tileRows %s", tileCols, tileRows)

        for c = 1, tileCols do
            for r = 1, tileRows do
                local bgTile = tools.img("ui_bg_main_tile.png", tileDimensions.width, tileDimensions.height)
                bgTile.x = bgTile.contentWidth * (c - 0.5) - tileGroupCenter.x
                bgTile.y = bgTile.contentHeight * (r - 0.5) - tileGroupCenter.y
                bgGrassGroup:insert(bgTile)
            end
        end
        bgGroup:insert(bgGrassGroup)

        -- paint pitch

        local paintingTop = tools.img("ui_bg_main_markings.png", 310, 190)
        paintingTop.yScale = -1
        paintingTop.x = properties.center.x
        paintingTop.y = properties.y + paintingTop.contentHeight * 0.5
        bgGroup:insert(paintingTop)

        local paintingBottom = tools.img("ui_bg_main_markings.png", 310, 190)
        paintingBottom.x = properties.center.x
        paintingBottom.y = properties.y + properties.height - paintingBottom.contentHeight * 0.5
        bgGroup:insert(paintingBottom)
        return bgGroup
    end

    local backgroundGroup = generateBackground()
    group:insert(backgroundGroup)

    -- key event
    local platformName = system.getInfo( "platformName" )

    local function onKeyEvent( event )
        local message = "Key '" .. event.keyName .. "' was pressed " .. event.phase

        if ( event.keyName == "back" and platformName == "Android" )
            or platformName ~= "Android" then

                scene.removeMe()
                Runtime:removeEventListener( "key", onKeyEvent )

                Runtime:dispatchEvent({ name = properties.playAudioMusic })
                composer.gotoScene( "code.startScreen" )
                composer.removeScene( "code.board" )

            return true
        end
    end

    Runtime:addEventListener( "key", onKeyEvent )

    -- controls

    local controlsOverlay = controlsClass.new()
    group:insert(controlsOverlay)



    local topDisc = display.newGroup()
    topDisc.x = properties.center.x
    topDisc.y = properties.y + topDisc.contentHeight * 0.5 + 100
    local topDiscImg = tools.img("game_player_enemy.png", 100, 40)
    topDisc:insert(topDiscImg)
    group:insert(topDisc)

    local function moveTopDisc(event)
        if topDisc.x + event.moveX < properties.x + topDisc.contentWidth * 0.5 then -- too far to left
        topDisc.x = properties.x + topDisc.contentWidth * 0.5
        elseif topDisc.x + event.moveX > properties.x + properties.width - topDisc.contentWidth * 0.5 then -- too far to right
        topDisc.x = properties.x + properties.width - topDisc.contentWidth * 0.5
        else
            topDisc.x = topDisc.x + event.moveX
        end
    end


    local function computerMoveTopDisc(event)
        local danger = (ball.y - topDisc.y) < (ball.x - topDisc.x) -- if ball is to close to disc and old transition is still in progress
        if (topDisc.computerTransition and not danger) or
                ball.y > properties.height * 0.5 or
                ball.y <= topDisc.y then
            return
        end

        local framesInterval = 10

        local frameX, frameTime = ball.getFrameXVelocityAndTime()
        if frameX == 0 then return end


        local moveX = ball.x + frameX * framesInterval
        local transitionTime = frameTime * framesInterval
        if transitionTime < p.difficultyLevel then transitionTime = p.difficultyLevel end

        if moveX < properties.x + topDisc.contentWidth * 0.5 then -- too far to left
            moveX = properties.x + topDisc.contentWidth * 0.5
        elseif moveX > properties.x + properties.width - topDisc.contentWidth * 0.5 then -- too far to right
            moveX = properties.x + properties.width - topDisc.contentWidth * 0.5
        end


        topDisc.computerTransition = transition.to(topDisc, {
            time = transitionTime,
            x = moveX,
            onComplete = function()
                if topDisc.computerTransition then transition.cancel(topDisc.computerTransition); topDisc.computerTransition = nil end
            end
        })
    end


    local function topDiscCollision(event)
--        shallowLogTable(event, 2)
        if event.phase == "began" then
            local sparks = specialEffects.burstSparks({ x = event.x, y = event.y, dirY = -1, dirX = nil, amount = 18 })
            group:insert(sparks)
            ball.bounced()
            Runtime:dispatchEvent({ name = properties.playAudioBounce1 })
        end
    end

    topDisc:addEventListener("collision", topDiscCollision)


    local bottomDisc = display.newGroup()
    bottomDisc.x = properties.center.x
    bottomDisc.y = properties.y + properties.height - bottomDisc.contentHeight * 0.5 - 100
    local btmDiscImg = tools.img("game_player_hero.png", 100, 40)
    bottomDisc:insert(btmDiscImg)
    group:insert(bottomDisc)

    local function moveBottomDisc(event)
        if bottomDisc.x + event.moveX < properties.x + bottomDisc.contentWidth * 0.5 then -- too far to left
        bottomDisc.x = properties.x + bottomDisc.contentWidth * 0.5
        elseif bottomDisc.x + event.moveX > properties.x + properties.width - bottomDisc.contentWidth * 0.5 then -- too far to right
        bottomDisc.x = properties.x + properties.width - bottomDisc.contentWidth * 0.5
        else
            bottomDisc.x = bottomDisc.x + event.moveX
        end
    end

    Runtime:addEventListener(properties.eventTypeMoveBottomDisc, moveBottomDisc)

    local function bottomDiscCollision(event)
--        shallowLogTable(event, 2)
        if event.phase == "began" then
            local sparks = specialEffects.burstSparks({ x = event.x, y = event.y, dirY = 1, dirX = nil, amount = 10 })
            group:insert(sparks)
            ball.bounced()
            Runtime:dispatchEvent({ name = properties.playAudioBounce1 })
        end
    end

    bottomDisc:addEventListener("collision", bottomDiscCollision)


    -- walls

    local leftWall = display.newRect(properties.x + 2, properties.center.y, 2, properties.height + 100)
    group:insert(leftWall)

    local rightWall = display.newRect(properties.x + properties.width - 2, properties.center.y, 2, properties.height + 100)
    group:insert(rightWall)

    -- score fields

    -- on top, for blue bottom player

    local function updateBottomPlayerScore(score)
        local paramsBottom = {
            score = score,
            time = 4000,
            delay = 20, -- between animation steps
            callback = nil,
            anchorPointX = 0,
            anchorPointY = nil,
            fontSize = 58,
            startDelay = 500,
            startValue = p.topPlayerScore or 0,
            color = properties.colorBlue,
            prefix = "Score: ",
            suffix = nil,
            sound = nil,
        }

        p.topPlayerScore = score

        if bottomScore then bottomScore.removeMe() end
        bottomScore = scoreWidget.new(paramsBottom)
        bottomScore.alpha = 0
        bottomScore.x = properties.center.x - properties.width * 0.45
        bottomScore.y = properties.center.y - properties.height * 0.25
        group:insert(bottomScore)

        transition.to(bottomScore, { alpha = 0.8 })
    end


    -- on bottom, for red top player
    local function updateTopPlayerScore(score)
        local paramsTop = {
            score = score,
            time = 4000,
            delay = 20, -- between animation steps
            callback = nil,
            anchorPointX = 0,
            anchorPointY = nil,
            fontSize = 58,
            startDelay = 500,
            startValue = p.bottomPlayerScore or 0,
            color = properties.colorRed,
            prefix = "Score: ",
            suffix = nil,
            sound = nil,
        }

        p.bottomPlayerScore = score


        if topScore then topScore.removeMe() end
        topScore = scoreWidget.new(paramsTop)
        topScore.alpha = 0
        topScore.rotation = 180
        topScore.x = properties.center.x + properties.width * 0.45
        topScore.y = properties.center.y + properties.height * 0.25
        group:insert(topScore)

        transition.to(topScore, { alpha = 0.8 })
    end

    updateBottomPlayerScore(0)
    updateTopPlayerScore(0)

    -- ball
    ball = ballClass.new()
    ball.x = properties.center.x
    ball.y = properties.center.y
    group:insert(ball)


    local bPars = {
        moveAdditionXMin = 40,
        moveAdditionXMax = 60,
        moveAdditionYMin = 20,
        moveAdditionYMax = 30,
        cycleTime = 200,
    }

    local ballStartCycles = 0
    local ballXdirection = 1
    local ballYdirection = 1
    local function startBallMovement(event)



        ballXdirection = ballXdirection * (random(2) == 1 and 1 or -1)

        if ballStartCycles < 5 then
            ballStartCycles = ballStartCycles + 1
        end

        if ball.startTransitionX then transition.cancel(ball.startTransitionX); ball.startTransitionX = nil end
        if ball.startTransitionY then transition.cancel(ball.startTransitionY); ball.startTransitionY = nil end


        ball.startTransitionX = transition.to(ball, {
            time = 800,
            x = properties.center.x + random(bPars.moveAdditionXMin, bPars.moveAdditionXMax * ballStartCycles) * ballXdirection,
            transition = easing.inOutCubic,
            onComplete = startBallMovement
        })

        ballYdirection = ballYdirection * -1


        ball.startTransitionY = transition.to(ball, {
            time = 800,
            y = properties.center.y + random(bPars.moveAdditionYMin, bPars.moveAdditionYMax) * ballYdirection * ballStartCycles * -1,
            transition = easing.inOutCubic,
        })
    end

    local topDiscShape = {
        -topDisc.width * 0.5, -topDisc.height * 0.5,
        topDisc.width * 0.5, -topDisc.height * 0.5,
        topDisc.width * 0.5, -topDisc.height * 0.05,
        topDisc.width * 0.25, topDisc.height * 0.15,
        0, topDisc.height * 0.20,
        -topDisc.width * 0.25, topDisc.height * 0.15,
        -topDisc.width * 0.5, -topDisc.height * 0.05,
    }

    local bottomDiscShape = {
        bottomDisc.width * 0.5, bottomDisc.height * 0.1,
        -bottomDisc.width * 0.5, bottomDisc.height * 0.1,
        -bottomDisc.width * 0.5, -bottomDisc.height * 0.35,
        -bottomDisc.width * 0.25, -bottomDisc.height * 0.55,
        0, -bottomDisc.height * 0.60,
        bottomDisc.width * 0.25, -bottomDisc.height * 0.55,
        bottomDisc.width * 0.5, -bottomDisc.height * 0.35,
    }

    board.releaseBall = function(event)
        log:debug("releaseBall")

        Runtime:removeEventListener(properties.eventTypeReleaseBall, board.releaseBall)

        if tapToStart then tapToStart.close() end
        ballStartCycles = 0
        physics.start()
        if properties.debugMode then physics.setDrawMode("hybrid") end -- to draw or not to draw rigidbody shape
        physics.setAverageCollisionPositions(true) -- to detect collision point in center of collision
        physics.setReportCollisionsInContentCoordinates(true)

        if ball.startTransitionX then transition.cancel(ball.startTransitionX); ball.startTransitionX = nil end
        if ball.startTransitionY then transition.cancel(ball.startTransitionY); ball.startTransitionY = nil end

        ball.bounced()

        transition.to(bottomScore, { alpha = 0 })
        transition.to(topScore, { alpha = 0 })

        physics.setGravity(0, 0)
        physics.addBody(ball, "dynamic", { density = 3.0, friction = 0, bounce = 1.0, radius = 20 })
        physics.addBody(topDisc, "static", { density = 3.0, friction = 0, bounce = 1.1, shape = topDiscShape })
        physics.addBody(bottomDisc, "static", { density = 3.0, friction = 0, bounce = 1.1, shape = bottomDiscShape })
        physics.addBody(leftWall, "static", { density = 3.0, friction = 0, bounce = 1.0 })
        physics.addBody(rightWall, "static", { density = 3.0, friction = 0, bounce = 1.0 })

        if p.gameType == "single" then -- so if we play with computer
            Runtime:addEventListener("enterFrame", computerMoveTopDisc)
        end

        ball:applyForce(500 * ballXdirection, 2500 * ballYdirection, ball.x, ball.y)
    end



    -- restart level

    board.restartLevel = function(event)
        log:debug("restartLevel")
        if event and event.gameType then
            p.gameType = event.gameType
        end

        ball.x = properties.center.x
        ball.y = properties.center.y
        Runtime:addEventListener(properties.eventTypeReleaseBall, board.releaseBall)
        Runtime:addEventListener(properties.eventTypeRoundOver, board.levelFinished)

        startBallMovement()
        tapToStart = specialEffects.tapToStartText({ noFlip = p.gameType ~= "hotSeat" })

        if p.gameType ~= "hotSeat" then -- so if only one player plays on a device
            Runtime:removeEventListener(properties.eventTypeMoveTopDisc, moveTopDisc)
        else
            Runtime:addEventListener(properties.eventTypeMoveTopDisc, moveTopDisc)
        end
    end

    Runtime:addEventListener(properties.eventTypeRestartLevel, board.restartLevel)


    -- level finished

    board.levelFinished = function(event)
        log:debug("levelFinished")
        physics.stop()
        Runtime:removeEventListener(properties.eventTypeRoundOver, board.levelFinished)

        if p.gameType == "single" then -- so if we play with computer
            Runtime:removeEventListener("enterFrame", computerMoveTopDisc)
        end

        if p.gameType == "hotSeat" or event.whoWon == "blue" then
            transition.to(topScore, { alpha = 0.5 })
            updateBottomPlayerScore(properties.scorePerWin )
            Runtime:dispatchEvent({ name = properties.playAudioSuccess }) -- always play success when two player plays
        else
            transition.to(bottomScore, { alpha = 0.5 })
            updateTopPlayerScore(properties.scorePerWin )
            Runtime:dispatchEvent({ name = properties.playAudioFailure }) -- play failure if red one has won
        end

        local finishText = specialEffects.simpleText({
            text = "Player " .. event.whoWon .. "\n You're Winner!", --ever played Big Rigs?
            size = 50,
            rotationsAmount = p.gameType ~= "hotSeat" and 0 or 3,
            callback = board.restartLevel
        })
        group:insert(finishText)
    end


    scene.removeMe = function()
        log:debug("scene.removeMe")
        physics.stop()
        if tapToStart then tapToStart.close() end
        topScore.removeMe()
        bottomScore.removeMe()
        Runtime:removeEventListener(properties.eventTypeMoveBottomDisc, moveBottomDisc)
        Runtime:removeEventListener("enterFrame", computerMoveTopDisc)
        Runtime:removeEventListener(properties.eventTypeReleaseBall, board.releaseBall)
        Runtime:removeEventListener(properties.eventTypeRoundOver, board.levelFinished)
        Runtime:removeEventListener(properties.eventTypeMoveTopDisc, moveTopDisc)
        Runtime:removeEventListener(properties.eventTypeRestartLevel, board.restartLevel)
    end

end

-- "board:show()"
function board:show(event)

    local boardGroup = self.view
    local phase = event.phase

--    log.table(event, 2)

    if (phase == "will") then
    elseif (phase == "did") then
        local gameType = event.params.gameType
        Runtime:dispatchEvent({ name = properties.eventTypeRestartLevel, gameType = gameType })

    end
end


-- "board:hide()"
function board:hide(event)

    local boardGroup = self.view
    local phase = event.phase

    if (phase == "will") then
    elseif (phase == "did") then
    end
end


-- "board:destroy()"
function board:destroy(event)

    local boardGroup = self.view

    if (phase == "will") then
    elseif (phase == "did") then
    end
end


-- -------------------------------------------------------------------------------

-- Listener setup
board:addEventListener("create", board)
board:addEventListener("show", board)
board:addEventListener("hide", board)
board:addEventListener("destroy", board)

-- -------------------------------------------------------------------------------

return board