
display.setStatusBar(display.HiddenStatusBar)

local properties = require("code.config.properties")
local composer = require("composer")
local audioSystemObject = require("code.tools.audioSystem")
local options = require("code.config.options")

system.activate("multitouch")
math.randomseed(os.time())

options.load()
local audioSystem = audioSystemObject.new()



local startGame = function()

    --    options.save("dupa", "giant dupa")
    --    log:debug("options %s", tostring(options.dupa))

    Runtime:dispatchEvent({ name = properties.playAudioMusic })

--        composer.gotoScene("code.startScreen")
    composer.gotoScene("code.credits")


end

startGame()